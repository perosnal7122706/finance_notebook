from datetime import date, datetime
from enum import Enum
from pydantic import BaseModel, ConfigDict


class Category(str, Enum):
    """
    Класс категории записи.
    """
    INCOME: str = "Доход"
    OUTCOME: str = "Расход"


class RecordCreate(BaseModel):
    """
    Базовая модель записи. Предназначена для создания записи, когда неизвестен id.

    Атрибуты:

        record_date (date) - дата записи
        category (Category) - категория записи (доход или расход)
        sum (int) - денежная сумма
        note (str | None) - заметка, опциональный параметр

    """
    model_config = ConfigDict(use_enum_values=True)

    record_date: date
    category: Category
    sum: int
    note: str | None = None


class Record(RecordCreate):
    """
        Модель записи. Представляет запись в базе.

        Атрибуты:

            id (int) - идентификационный номер записи (первичный ключ)
            record_date (date) - дата записи
            category (Category) - категория записи (доход или расход)
            sum (int) - денежная сумма
            note (str | None) - заметка, опциональный параметр

        """
    id: int


class FinanceNotebook:
    """
    Класс, реализующий программу доступа к финансовому кошельку

    Публичный метод:

        run() - запуск программы
    """

    def __create_record__(self, record: RecordCreate) -> str:
        """
        Создание записи.

        Параметры:
            record (RecordCreate) -

        Результат:
            (str) - Сообщение о создании записи
        """
        record_id = 0

        with open("index.txt", "r") as file:
            record_id = int(file.readline()) + 1

        with open("index.txt", "w") as file:
            file.write(f"{record_id}")

        with open("FinanceNote.txt", "a") as file:
            record_write = Record(id=record_id, **record.model_dump())
            file.write(record_write.model_dump_json() + "\n")

        return "Запись создана"

    def __get_records__(self,
                        id: int | None = None, record_date: date | None = None,
                        category: Category | None = None, sum: int | None = None) -> list[Record]:
        """
        Поиск записей.

        Параметры:

            id (int | None)- идентификационный номер записи (первичный ключ).
                Если поиск осуществляется по id, то остальные параметры не учитываются
            record_date (date | None) - дата записи
            category (Category | None) - категория записи (доход или расход)
            sum (int | None) - денежная сумма
            note (str | None) - заметка, опциональный параметр

        Результат:
            (list[Record]) - список найденных записей
        """


        result: list[Record] = []
        with open("FinanceNote.txt", "r") as file:
            for line in file.readlines():
                record = Record.model_validate_json(line)

                if id is not None:
                    if record.id == id:
                        return [record]
                    else:
                        continue

                if record_date is not None and record.record_date != record_date:
                    continue
                if category is not None and record.category != category:
                    continue
                if sum is not None and record.sum != sum:
                    continue

                result.append(record)

        return result

    def __update_record__(self, record: Record) -> str:
        """
        Изменение записи.

        Параметры:
            record (Record) - запись к изменению. Поиск осуществляется по id

        Результат:
            (str) - Сообщение об изменении записи
        """
        with open("index.txt", "r") as file:
            record_id = int(file.readline())
            if record.id > record_id:
                return "Запись не найдена"

        with open("FinanceNote.txt", "r") as files:
            buffer = files.readlines()
            buffer[record.id - 1] = record.model_dump_json() + "\n"

            with open("FinanceNote.txt", "w") as file:
                file.writelines(buffer)

        return "Успешно обновлена"

    def __count_balance__(self) -> int:
        """
        Вывод баланса.

        Результат:
            (int) - Баланс на счету
        """
        sum = 0

        with open("FinanceNote.txt", "r") as file:
            for line in file.readlines():
                record = Record.model_validate_json(line)

                if record.category == Category.INCOME:
                    sum += record.sum
                else:
                    sum -= record.sum

        return sum

    def __validate_category__(self, category_num: int) -> Category | None:
        """
        Проверка категории записи. Вспомогательная функция

        Параметры:
            category_num (int) - номер категории записи. Доход - 0, Расход - 1

        Результат:
            (Category) - категория записи
        """
        if category_num == 0:
            return Category.INCOME
        elif category_num == 1:
            return Category.OUTCOME
        else:
            print("\tНеверная категория\n")
            return None

    def run(self) -> None:
        """
        Функция запуска программы.
        Представляет из себя интерактивное меню в командной строке
        """
        option = 0
        while option != 4:
            print("Ведите номер операции:\n"
                  "\t0: Вывод баланса\n"
                  "\t1: Добавить запись\n"
                  "\t2: Поиск записи\n"
                  "\t3: Изменение записи\n"
                  "\t4: Выход\n")
            option = int(input("Номер операции: "))

            # Вывод баланса
            if option == 0:
                print(f"\nТекущий баланс: {self.__count_balance__()}\n")

            # Создание записи
            elif option == 1:
                print("\nВведите данные записи:\n")

                date = input("\tДата в формате ГГ-ММ-ДД: ")
                category = int(input("\tКатегория (Доход - 0, Расход - 1): "))
                sum = int(input("\tСумма: "))
                note = input("\tЗаметка: ")

                category = self.__validate_category__(category)

                record = RecordCreate(record_date=date, category=category,
                                      sum=sum, note=note)

                msg = self.__create_record__(record)
                print(msg + "\n")

            # Поиск записи
            elif option == 2:

                print("Введите данные записи\n"
                      "При отсутсвии параметра пропустите ввод\n"
                      "При вводе id остальные парамаетры не требуются\n")

                id = input("\tВведите id: ")
                # Поиск не по id
                if id == "":
                    record_date = input("\tДата в формате ГГ-ММ-ДД: ")
                    category = input("\tКатегория (Доход - 0, Расход - 1): ")
                    sum = input("\tСумма: ")

                    # Преобразуем параметры поиска  нужному формату
                    record_date = None if record_date == '' else datetime.strptime(record_date, "%Y-%m-%d").date()
                    category = None if category == '' else self.__validate_category__(int(category))
                    sum = None if sum == '' else int(sum)
                    id = None

                    result = self.__get_records__(id, record_date, category, sum)

                # Поиск по id
                else:
                    id = int(id)
                    result = self.__get_records__(id, None, None, None)

                print("\tНайденные записи:\n")
                for record in result:
                    print(f"\t\t{record}\n")

            # Изменение записи
            elif option == 3:
                print("\tВведите параметры записи: ")

                id = int(input("\t\tid записи: "))
                record_date = input("\t\tДата в формате ГГ-ММ-ДД: ")
                category = int(input("\t\tКатегория (Доход - 0, Расход - 1): "))
                sum = int(input("\t\tСумма: "))
                note = input("\t\tЗаметка: ")

                record_date = datetime.strptime(record_date, "%Y-%m-%d").date()
                category = self.__validate_category__(category)

                record = Record(id=id, record_date=record_date, category=category,
                                sum=sum, note=note)

                msg = self.__update_record__(record)
                print("\t\t" + msg + "\n")

            # Выход
            elif option == 4:
                print("Завершение работы\n")
                return

            # Неверный выбор
            else:
                print("Ошибка! Некорректный выбор\n")


app = FinanceNotebook()

app.run()
