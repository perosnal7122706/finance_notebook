# Личный финансовый кошелёк

## Техническое задание
    Основные возможности:
    1. Вывод баланса: Показать текущий баланс, а также отдельно доходы и расходы.
    2. Добавление записи: Возможность добавления новой записи о доходе или расходе.
    3. Редактирование записи: Изменение существующих записей о доходах и расходах.
    4. Поиск по записям: Поиск записей по категории, дате или сумме.

    Требования к программе:
    1. Интерфейс: Реализация через консоль (CLI), без использования веб- или графического интерфейса (также без использования фреймворков таких как Django, FastAPI, Flask  и тд).
    2. Хранение данных: Данные должны храниться в текстовом файле. Формат файла определяется разработчиком.
    3. Информация в записях: Каждая запись должна содержать дату, категорию (доход/расход), сумму, описание (возможны дополнительные поля).

## Описание проекта

    Проект состоит из трёх файлов:
    1. main.py - исходный ход программы
    2. FinanceNote.txt - файл с записями
    3. index.txt - файл с количеством записей в базе, нужен для индексации

    Содержание main.py:

    Основной класс - FinanceNotebook. Содержит весь функционал для работы с записями в кошельке в приватных методах.
    Единственный публичный метод - run(), он запускает интерактивную часть программы в командной строке.
    Документация по методам и классам приведена в doctring